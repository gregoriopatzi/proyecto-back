const jwt = require('jsonwebtoken'); // se importa la libreria para el token

// verifica la autenticacion
const verificarAuth = (req, res, next) => {
  // recibe el token
  const token = req.get('token');

  // decodifica el token, clave secreta
  jwt.verify(token, 'secret', (err, decoded) => {

    // verifica si existe algun error
    if(err){
      // retorn el mensaje de error
      return res.status(401).json({
        mensaje: 'Usuario no válido'
      })
    }

    // decodifica los datos del usuario
    req.usuario = decoded.data;

    next(); // para a la siguiente accion

  })

}
// verifica si es administrador el usuario creado
const verificarAdministrador = (req, res, next) => {

  const rol = req.usuario.role // recibe el rol
  
  // verifica si es admin
  if(rol === 'ADMIN'){
    next(); // pasa a la siguiente accion
  }else{
    // mensaje de error
    return res.status(401).json({
      mensaje: 'Usuario no válido'
    })
  }

}

module.exports = {verificarAuth, verificarAdministrador}