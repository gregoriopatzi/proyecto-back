import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import path from 'path';

const app = express(); //frameworl para node.js

// Conexion a DB
const mongoose = require('mongoose');
// const uri = 'mongodb://localhost:27017/proyectos_bd';

// Conexion en la nube
const uri = 'mongodb+srv://user_mongo:EpPpMxHd4WlBMaDb@proyecto-6.os2c9aa.mongodb.net/?retryWrites=true&w=majority';

const options = {
  useNewUrlParser: true, useCreateIndex: true,  useUnifiedTopology: true
}

// verificacion para la conexion a la base de datos
mongoose.connect(uri, options).then(
  () => { console.log('Conectado a mongoDB'); },
  err => { err }
);

// Middleware
app.use(morgan('tiny'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Rutas
// app.get('/', (req, res) => {
//   res.send('Hello World!');
// });

// rutas de la aplicacion
app.use('/api', require('./routes/noticias'));
app.use('/api', require('./routes/categoria'));
app.use('/api', require('./routes/user'));
app.use('/api/login', require('./routes/login'));

// Middleware para Vue.js router modo history
const history = require('connect-history-api-fallback');
app.use(history());
app.use(express.static(path.join(__dirname, 'public'))); // direcciona a la pagina principal de la aplicacion del front compilado

// redirecciona el puerdo en el cual se levanta el servidor de express
app.set('puerto', process.env.PORT || 3000);
app.listen(app.get('puerto'), () => {
  console.log('Example app listening on port'+ app.get('puerto')); // mensaje de confirmacion que la aplicacion esta funcionando
});