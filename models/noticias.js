import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const noticiasSchema = new Schema({

  nombre: {type: String, required: [true, 'Nombre obligatorio']},
  descripcion: String,
  fecha:Date,
  usuarioId: String,
  date:{type: Date, default: Date.now},
  activo: {type: Boolean, default: true}

});

// Convertir a un modelo 
const Noticia = mongoose.model('Noticia', noticiasSchema);

export default Noticia;