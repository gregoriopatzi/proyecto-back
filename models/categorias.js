import mongoose from 'mongoose'; // se importa la libreria de mongo
const Schema = mongoose.Schema; // se crea el esquema para la tabla

// nuevo esquema
// se crea las columnas con los campos de la tabla seguido del tipo de dato, si es requerido y mensaje para la validacion
const categoriaSchema = new Schema({

  nombre: {type: String, required: [true, 'Nombre obligatorio']},
  descripcion: String,
  date:{type: Date, default: Date.now},
  activo: {type: Boolean, default: true}

});

// Convertir a un modelo 
const Categoria = mongoose.model('Categoria', categoriaSchema);

export default Categoria;