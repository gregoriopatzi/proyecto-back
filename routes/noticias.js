import express from 'express';
const router = express.Router();

// importar el modelo noticia
import Noticia from '../models/noticias';

const {verificarAuth, verificarAdministrador} = require('../middlewares/autenticacion')

// Agregar una noticia
router.post('/nueva-noticia', [verificarAuth, verificarAdministrador], async(req, res) => {
  const body = req.body;
  // console.log(req.body);
  // body.usuarioId = req.usuario._id;
  try {
    const noticiaDB = await Noticia.create(body);
    res.status(200).json(noticiaDB); 
  } catch (error) {
    return res.status(500).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

// Get con parámetros
router.get('/noticia/:id', [verificarAuth, verificarAdministrador], async(req, res) => {
  const _id = req.params.id;
  try {
    const noticiaDB = await Noticia.findOne({_id});
    res.json(noticiaDB);
  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

// Get con todos los documentos
router.get('/noticia', async(req, res) => {
  try {
    const noticiaDb = await Noticia.find();
    res.json(noticiaDb);
  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

// Delete eliminar una noticia
router.delete('/noticia/:id', [verificarAuth, verificarAdministrador], async(req, res) => {
  const _id = req.params.id;
  try {
    const noticiaDb = await Noticia.findByIdAndDelete({_id});
    if(!noticiaDb){
      return res.status(400).json({
        mensaje: 'No se encontró el id indicado',
        error
      })
    }
    res.json(noticiaDb);  
  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

// Put actualizar una noticia
router.put('/noticia/:id', [verificarAuth, verificarAdministrador], async(req, res) => {
  const _id = req.params.id;
  const body = req.body;
  try {
    const noticiaDb = await Noticia.findByIdAndUpdate(
      _id,
      body,
      {new: true});
    res.json(noticiaDb);  
  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

router.post('/noticia/contar', async(req, res) => {
  try {
    const noticiaDb = await Noticia.find().count();
    res.json(noticiaDb);
  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

// Exportación de router
module.exports = router;