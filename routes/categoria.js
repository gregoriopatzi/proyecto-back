import express from 'express';
const router = express.Router(); // integra las rutas de la aplicacion

import Categoria from '../models/categorias'; // integra el modelo correspondiente

const {verificarAuth, verificarAdministrador} = require('../middlewares/autenticacion')

// peticion para crear una nueva categoria
router.post('/nueva-categoria', [verificarAuth, verificarAdministrador], async(req, res) => {
  const body = req.body;  
  try {
    const categoriaDB = await Categoria.create(body); // metodo para crear una nueva categoria
    res.status(200).json(categoriaDB); // respuesta positica al front
  } catch (error) {
    // retorna un mensaje de error al front
    return res.status(500).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

// identifica una categoria
router.get('/categoria/:id', [verificarAuth, verificarAdministrador], async(req, res) => {
  const _id = req.params.id; // recupera el id que llega del front
  try {
    const categoriaDB = await Categoria.findOne({_id}); // busca en la tabla categoria, la categoria con el id mensionado
    res.json(categoriaDB); // envia la categoria encontrada al front
  } catch (error) {
    // mensaje de error
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

// lista todas las categorias
router.get('/categoria', async(req, res) => {
  try {
    const categoriaDb = await Categoria.find(); // metodo para listar todas las categorias
    res.json(categoriaDb); // envia ese listado al front
  } catch (error) {
    // mensaje de error
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

// elimina una categoria especificada por el id
router.delete('/categoria/:id', [verificarAuth, verificarAdministrador], async(req, res) => {
  const _id = req.params.id; // se recibe el id a eliminar
  try {
    const categoriaDb = await Categoria.findByIdAndDelete({_id}); // metodo para eliminar la categoria
    // verifica que la respuesta sea distinta es falsa
    if(!categoriaDb){
      // retorna el mensaje de error
      return res.status(400).json({
        mensaje: 'No se encontró el id indicado',
        error
      })
    }
    res.json(categoriaDb); // envia true al front como respuesta de la eliminacion 
  } catch (error) {
    // mensaje de error
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

// edita una categoria especificada por el id
router.put('/categoria/:id', [verificarAuth, verificarAdministrador], async(req, res) => {
  const _id = req.params.id; // recibe el id
  const body = req.body; // recibe los datos a modificar de la categoria
  try {
    const categoriaDb = await Categoria.findByIdAndUpdate(
      _id,
      body,
      {new: true}); // editar la categoria por el id enviado, y con el nuevo contenido
    res.json(categoriaDb);  
  } catch (error) {
    // mensaje de error
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

// cuenta las categorias para el dashboard
router.post('/categoria/contar', async(req, res) => {
  try {
    const categoriaDb = await Categoria.find().count(); // cuenta el numero de categorias creadas
    res.json(categoriaDb); // envia el contador al front
  } catch (error) {
    // mensahe de error
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

module.exports = router;